#include<iostream>
#include <stdio.h>
#include <winsock2.h>
#include <stdlib.h>
using namespace std;

void check(int A[5][5], int k) {
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            if (A[i][j] == k) {
                A[i][j] = 42;
            }
        }
    }
}

void print(int A[5][5]) {
    system("CLS");
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            cout << (char) A[i][j] << "     ";
        }
        cout << "\n\n\n";
    }
}

int check1(int A[5][5], int a) {
    if (a < 65 || a > 89) {
        return 0;
    }
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            if ((int) a == A[i][j]) {
                return 0;
            }
        }
    }
    return 1;
}

int check2(int A[5][5], int a) {
    if (a < 65 || a > 89) {
        return 0;
    }
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            if ((int) a == A[i][j]) {
                return 1;
            }
        }
    }
    return 0;
}

void nhap(int A[5][5]) {
    char a = char(0);
    for(int i = 0; i < 25; i++){
            cout<<(char) (i+65) << " ";
    }
    cout<<"\n";
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            cout << "A[" << i << "][" << j << "]: ";
            cin>>a;
            a = toupper(a);
            if (check1(A, (int) a) == 0) {
                cout << "Nhap lai \n";
                j--;

            } else {
                A[i][j] = (int) a;
            }
            print(A);
            for(int i = 0; i < 25; i++){
                    cout<<(char) (i+65) << " ";
            }
             cout<<"\n";
        }
    }


}

int server(int A[5][5]);
int client(int A[5][5]);

int check(int A[5][5]) {
    int sum1 = 0, sum2 = 0, sum3 = 0, flag1 = 0, flag2 = 0;
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            sum1 += A[i][j];
            sum2 += A[j][i];
        }
        //cout << "Sum1: " << sum1 << "\n";
        //cout << "Sum2: " << sum2 << "\n";
        if (sum1 == (42 * 5)) {
            flag1 = 1;
            //break;
        }
        if (sum2 == (42 * 5)) {
            flag2 = 1;
            //break;
        }
        //cout << "Flag1: " << flag1 << "\n";
        //cout << "Flag2: " << flag2 << "\n";
        if (flag1 == 1 && flag2 == 1) {
            break;
        }
        sum1 = 0;
        sum2 = 0;
    }
    if (A[0][0] + A[1][1] + A[2][2] + A[3][3] + A[4][4] == (42 * 5) || A[4][0] + A[3][1] + A[2][2] + A[1][3] + A[0][4] == (42 * 5)) {
        cout << "C: " << 1 << "\n";
        return (flag1 & flag2);
    }
    return 0;
}

void win() {
    //system("CLS");
    cout << "Winnnnnnnnnnnnnn........... \n";
    WSACleanup();
    system("Pause");
}

void lose() {
    //system("CLS");
    cout << "Loseeeeeeeeeeeee........... \n";
    WSACleanup();
    system("Pause");
}

int main() {
    int A[5][5];
    int k = 0, type = 0;

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            A[i][j] = 1;
        }
    }
    print(A);
    nhap(A);


    //nhap(A);
    cout << "1-Server \n";
    cout << "2-Client \n";
    cin>>type;
    switch (type) {
        case 1:
            server(A);
            break;
        case 2:
            client(A);
            break;
        default:
            cout << "Chon lai!";
            break;
    }
    system("Pause");
}

int client(int A[5][5]) {
    while (1) {
        // Initialize Winsock
        WSADATA wsaData;
        //int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
        int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);

        if (iResult != NO_ERROR) {
        }            //printf("Client: Error at WSAStartup().\n");
        else {
        }
        //printf();
        //printf("Client: WSAStartup() is OK.\n");

        // Create a SOCKET for connecting to server
        SOCKET ConnectSocket;
        ConnectSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
        if (ConnectSocket == INVALID_SOCKET) {
            //printf("Client: Error at socket(): %ld.\n", WSAGetLastError());
            system("Pause");
            WSACleanup();

            return 0;
        } else {
        }
        //printf("Client: socket() is OK.\n");

        // The sockaddr_in structure specifies the address family,
        // IP address, and port of the server to be connected to.
        sockaddr_in clientService;
        clientService.sin_family = AF_INET;
        clientService.sin_addr.s_addr = inet_addr("10.30.0.209");
        clientService.sin_port = htons(8888);
        // Connect to server.
        if (connect(ConnectSocket, (SOCKADDR*) & clientService, sizeof (clientService)) == SOCKET_ERROR) {
            //printf("Client: Failed to connect.\n");
            WSACleanup();
            system("Pause");
            return 0;
        } else {
            cout << "OK \n";
        }
        //printf("Client: connect() is OK.\n");
        // Declare and initialize variables.
        int bytesSent;
        int bytesRecv = SOCKET_ERROR;
        //string my_string1 = "ten chars.";

        //char sendbuf[100] = "Client: Sending some data.";
        char sendbuf[100] = "";
        //sendbuf[0] = (char) A[0][0];
        char a;
        //nhap(A);
        cout << "Ki tu: ";
        cin>>a;
        a = toupper(a);
        while (check2(A, (int) a) == 0) {
            cout << "Nhap lai: ";
            cin>>a;
            a = toupper(a);

        }
        check(A, (int) a);
        char recvbuf[100] = "";
        sendbuf[0] = a;
        //cout << "Send: " << a << "\n";
        //check(A, (int) a);
        if (check(A) == 1) {
            a = (char) 1;
            sendbuf[0] = a;
            bytesSent = send(ConnectSocket, sendbuf, strlen(sendbuf), 0);
            win();

        } else {
               
            bytesSent = send(ConnectSocket, sendbuf, strlen(sendbuf), 0);
            cout<<"Sent!";
            //Sleep(500);

        }

        //system("CLS");
        //cout << "Check: " << check(A) << "\n";

        print(A);
        //printf("Client: Bytes sent: %ld\n", bytesSent);
        while (bytesRecv == SOCKET_ERROR) {
            bytesRecv = recv(ConnectSocket, recvbuf, 100, 0);
            if ((int) recvbuf[0] == 1) {
                lose();
                //system("Pause");
            }
            if (bytesRecv == 0 || bytesRecv == WSAECONNRESET) {
                printf("Client: Connection Closed.\n");
                //Sleep(2000);
                break;
            } else {
                check(A, (int) recvbuf[0]);
                if (check(A) == 1) {
                    a = (char) 1;
                    sendbuf[0] = a;
                    bytesSent = send(ConnectSocket, sendbuf, strlen(sendbuf), 0);
                    win();
                    //system("Pause");
                }
                //system("CLS");
                //cout << "Check: " << check(A) << "\n";

                print(A);
                //printf("Client: recv() is OK.\n");
                //printf("Client: Bytes received: %ld\n", bytesRecv);
                //printf("Client: Received: %s\n", recvbuf);
                printf("Received: %s\n", recvbuf);
                Sleep(500);
                break;

            }
        }

        //WSACleanup();
    }
}

int server(int A[5][5]) {
    while (1) {
        WORD wVersionRequested;
        WSADATA wsaData;
        int wsaerr;

        // Using MAKEWORD macro, Winsock version request 2.2
        wVersionRequested = MAKEWORD(2, 2);

        wsaerr = WSAStartup(wVersionRequested, &wsaData);
        //while (1) {
        if (wsaerr != 0) {
            /* Tell the user that we could not find a usable WinSock DLL.*/
            //printf("Server: The Winsock dll not found!\n");
            return 0;
        } else {
            //printf("Server: The Winsock dll found!\n");
            //printf("Server: The status: %s.\n", wsaData.szSystemStatus);
        }

        /* Confirm that the WinSock DLL supports 2.2.*/
        /* Note that if the DLL supports versions greater    */
        /* than 2.2 in addition to 2.2, it will still return */
        /* 2.2 in wVersion since that is the version we      */
        /* requested.                                        */

        if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 2) {
            /* Tell the user that we could not find a usable WinSock DLL.*/
            //printf("Server: The dll do not support the Winsock version %u.%u!\n", LOBYTE(wsaData.wVersion), HIBYTE(wsaData.wVersion));
            WSACleanup();
            return 0;
        } else {
            //printf("Server: The dll supports the Winsock version %u.%u!\n", LOBYTE(wsaData.wVersion), HIBYTE(wsaData.wVersion));
            //printf("Server: The highest version this dll can support: %u.%u\n", LOBYTE(wsaData.wHighVersion), HIBYTE(wsaData.wHighVersion));
        }
        //////////Create a socket////////////////////////
        //Create a SOCKET object called m_socket.
        SOCKET m_socket;

        // Call the socket function and return its value to the m_socket variable.
        // For this application, use the Internet address family, streaming sockets, and
        // the TCP/IP protocol.
        // using AF_INET family, TCP socket type and protocol of the AF_INET - IPv4
        m_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

        // Check for errors to ensure that the socket is a valid socket.
        if (m_socket == INVALID_SOCKET) {
            //printf("Server: Error at socket(): %ld\n", WSAGetLastError());
            WSACleanup();
            return 0;
        } else {
            //printf("Server: socket() is OK!\n");
        }
        //while (1) {
        ////////////////bind//////////////////////////////
        // Create a sockaddr_in object and set its values.
        sockaddr_in service;

        // AF_INET is the Internet address family.
        service.sin_family = AF_INET;
        // "127.0.0.1" is the local IP address to which the socket will be bound.
        //service.sin_addr.s_addr = inet_addr("127.0.0.1");
        service.sin_addr.s_addr = inet_addr("10.30.0.209");
        // 55555 is the port number to which the socket will be bound.
        service.sin_port = htons(8888);
        //while (1)
        //{   
        // Call the bind function, passing the created socket and the sockaddr_in structure as parameters.
        // Check for general errors.
        //while (1) {

        if (bind(m_socket, (SOCKADDR*) & service, sizeof (service)) == SOCKET_ERROR) {
            //printf("Server: bind() failed: %ld.\n", WSAGetLastError());
            closesocket(m_socket);
            return 0;
        } else {
            //printf("Server: bind() is OK!\n");
        }

        //while (1) {
        // Call the listen function, passing the created socket and the maximum number of allowed
        // connections to accept as parameters. Check for general errors.
        if (listen(m_socket, 10) == SOCKET_ERROR) {
        }            //printf("Server: listen(): Error listening on socket %ld.\n", WSAGetLastError());
        else {
            //printf("Server: listen() is OK, I'm waiting for connections...\n");
        }
        //nhap(A);
        // Create a temporary SOCKET object called AcceptSocket for accepting connections.
        //SOCKET AcceptSocket;

        // Create a continuous loop that checks for connections requests. If a connection
        // request occurs, call the accept function to handle the request.
        // printf("Server: Waiting for a client to connect...\n");
        //printf("***Hint: Server is ready...run your client program...***\n");
        // Do some verification...
        // while (1){
        //{     
        //printf("Server: Ready!\n");
        SOCKET AcceptSocket;
        AcceptSocket = SOCKET_ERROR;

        while (AcceptSocket == SOCKET_ERROR) {
            AcceptSocket = accept(m_socket, NULL, NULL);
        }
        //while(1){
        // else, accept the connection...
        // When the client connection has been accepted, transfer control from the
        // temporary socket to the original socket and stop checking for new connections.
        //printf("Server: Client Connected!\n");
        m_socket = AcceptSocket;
        //break;
        //}

        int bytesSent;
        int bytesRecv = SOCKET_ERROR;
        //char sendbuf[200] = "This string is a test data from server";
        char sendbuf[200] = "";
        // initialize to empty data...
        char recvbuf[200] = "";

        // Send some test string to client...
        //printf("Server: Sending some test data to client...\n");

        //bytesSent = send(m_socket, sendbuf, strlen(sendbuf), 0);

        //if (bytesSent == SOCKET_ERROR)
        //       printf("Server: send() error %ld.\n", WSAGetLastError());
        //else
        //{
        //       printf("Server: send() is OK.\n");
        //       printf("Server: Bytes Sent: %ld.\n", bytesSent);
        //}

        // Receives some test string from client...and client
        // must send something lol...
        //while(1){
        bytesRecv = recv(m_socket, recvbuf, 200, 0);
        /*
        if((char) recvbuf[0] == 1){
                  lose();
        }
        
        check(A, (int) recvbuf[0]);
        if(check(A) == 1){
                    
                    char a = (char) 1;
                    sendbuf[0] = a;
                     cout << "Send: " << a << "\n";
                     bytesSent = send(m_socket, sendbuf, strlen(recvbuf), 0);
                     win();
                      //system("Pause");
                    
        }
        system("CLS");
         */
        //cout << "Check: " << check(A) << "\n";

        print(A);
        if (bytesRecv == SOCKET_ERROR) {
        }            //printf("Server: recv() error %ld.\n", WSAGetLastError());
        else {
            if ((char) recvbuf[0] == 1) {
                lose();
            }

            check(A, (int) recvbuf[0]);
            if (check(A) == 1) {

                char a = (char) 1;
                sendbuf[0] = a;
                cout << "Send: " << a << "\n";
                bytesSent = send(m_socket, sendbuf, strlen(recvbuf), 0);
                win();
                //system("Pause");

            }
            print(A);
            //system("CLS");
            printf("Received: \"%s\"\n", recvbuf);
            char a;
            cout << "Ky tu: ";
            cin>>a;
            a = toupper(a);
            while (check2(A, (int) a) == 0) {
                cout << "Nhap lai: ";
                cin>>a;
                a = toupper(a);

            }
            sendbuf[0] = a;
            cout << "Send: " << a << "\n";
            check(A, (int) sendbuf[0]);
            if (check(A) == 1) {

                a = (char) 1;
                sendbuf[0] = a;
                cout << "Send: " << a << "\n";
                bytesSent = send(m_socket, sendbuf, strlen(recvbuf), 0);
                win();
                //system("Pause");

            } else {
                bytesSent = send(m_socket, sendbuf, strlen(recvbuf), 0);
                 cout<<"Sent: %s\n"<<sendbuf;
                
            }

            //system("CLS");
            cout << "Check: " << check(A) << "\n";

            //system("ping -t google.com");
            print(A);
            //printf("Server: recv() is OK.\n");
            //printf("Server: Received data is: \"%s\"\n", recvbuf);
            //printf("Server: Bytes received: %ld.\n", bytesRecv);
        }


        //system("Pause");
        WSACleanup();
    }
}



